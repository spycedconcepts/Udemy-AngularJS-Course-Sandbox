import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer: boolean = false;
  serverCreationStatus: string = 'No server was created!';
  serverName: string = 'Test Server';
  userName: string = '';
  allowClearUserName: boolean = false;

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
   }

  ngOnInit() {
  }

  onCreateServer() {
    this.serverCreationStatus = `Server was created. Name is ${this.serverName}`;
  }

  onUpdateServerName(event: any) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }

  onUpdateUserName(event: any) {
    const inputValue = (<HTMLInputElement>event.target).value
    if(inputValue) {
      this.allowClearUserName = true;
    }
    console.log(inputValue);
  }

  onClickClearUsername() {
    this.userName = '';
    this.allowClearUserName = !this.allowClearUserName;
  }

}
